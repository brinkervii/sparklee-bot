import {Client as DiscordClient, Intents} from 'discord.js';
import {config as configureDotenv} from 'dotenv';
import registerBotModules from './bot-modules';
import {BotModulesService} from './services/bot-modules.service';
import {rootLogger} from './logging';

const log = rootLogger.child({component: 'index.ts'});

// Environment setup

configureDotenv({encoding: 'UTF-8'});

const nodeEnv = process.env.NODE_ENV || 'development';
log.info(`Node environment: ${nodeEnv}`);

// Discord setup

const client = new DiscordClient({
    partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS
    ]
});

// Configure modules

registerBotModules();

const botModuleService = new BotModulesService();

// Core

client.on('message', async message => {
    // Guard clauses
    if (message.author.id === client.user.id) {
        // Ignore messages sent by ourself
        return;
    }

    if (!message.guild) {
        // Do no run in direct messages
        return;
    }

    const work = [];

    // Bot modules
    work.push(botModuleService.dispatchMessage(message));

    // Execute all work
    await Promise.all(work);
});

client.on('messageReactionAdd', async (reaction, user) => {
    if (reaction.partial) {
        await reaction.fetch();
    }

    await botModuleService.dispatchReactionAdded(reaction, user);
});

client.on('messageReactionRemove', async (reaction, user) => {
    if (reaction.partial) {
        await reaction.fetch();
    }

    await botModuleService.dispatchReactionRemoved(reaction, user);
});

const startBot = async () => {
    await client.login(process.env.SPARKLEE_BOT_TOKEN);
};

startBot()
    .then(() => {
        log.info(`Successfully started the bot with bot user '${client.user.tag}'!`);
    })
    .catch(err => {
        console.error('Failed to start ye olde Discord bot!');
        console.error(err.toString());
    });
