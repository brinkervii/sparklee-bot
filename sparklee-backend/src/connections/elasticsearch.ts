import { Client } from '@elastic/elasticsearch';

let client = null;

const createClient = (): Client => {
    return new Client({
        node: process.env.ELASTICSEARCH_HOST
    });
};

export const es = (): Client => {
    if (!client) client = createClient();

    return client;
};
