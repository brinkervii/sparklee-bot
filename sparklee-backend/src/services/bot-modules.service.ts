import {BotModule, BotModuleFlowControl, BotModuleSpecification} from '../bot-modules/bot-module';
import {Message, MessageReaction, PartialMessageReaction, PartialUser, User} from 'discord.js';
import {rootLogger} from '../logging';

const log = rootLogger.child({component: 'bot-modules.service.ts'});

const registeredModules: Array<BotModuleSpecification> = [];

export class BotModulesService {
    private instances: Map<string, BotModule> = new Map<string, BotModule>();

    private getModuleInstance(key: string): BotModule {
        if (this.instances.has(key)) {
            return this.instances.get(key);
        }

        for (const spec of registeredModules) {
            if (spec.id === key) {
                const instance = spec.module();
                this.instances.set(spec.id, instance);

                return instance;
            }
        }

        throw new Error(`No such bot module: ${key}`);
    }

    public moduleById(id: string): BotModuleSpecification | null {
        for (const module of registeredModules) {
            if (module.id === id) {
                return module;
            }
        }

        return null;
    }

    public registerModule(spec: BotModuleSpecification): void {
        if (this.moduleById(spec.id)) {
            throw new Error(`Module id ${spec.id} is already registered!`);
        }

        registeredModules.push(spec);
    }

    public async dispatchMessage(message: Message): Promise<void> {
        const control = new BotModuleFlowControl();

        for (const spec of registeredModules) {
            const instance = this.getModuleInstance(spec.id);

            try {
                await instance.onMessage(message, control);
            } catch (e) {
                log.error(e);
            }

            if (!control.continueDispatch) {
                break;
            }
        }
    }

    public async dispatchReactionAdded(reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser): Promise<void> {
        for (const spec of registeredModules) {
            const instance = this.getModuleInstance(spec.id);

            try {
                await instance.onReactionAdded(reaction, user);

            } catch (e) {
                log.error(e);
            }
        }
    }

    public async dispatchReactionRemoved(reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser): Promise<void> {
        for (const spec of registeredModules) {
            const instance = this.getModuleInstance(spec.id);

            try {
                await instance.onReactionRemoved(reaction, user);

            } catch (e) {
                log.error(e);
            }
        }
    }

    public listRegisteredModules(): Array<BotModuleSpecification> {
        return [...registeredModules];
    }
}
