import { CommandParser } from './command-parser';

test('Find a command', () => {
    const command = 'ls';
    const testCommand = 'ls';

    const parser = new CommandParser();
    const result = parser.parse(command);

    expect(result.isCommand(testCommand)).toBeTruthy();
});

test('Respect casing with default parameters', () => {
    const command = 'MyCommand';
    const testCommand = 'myCommand';

    const parser = new CommandParser();
    const result = parser.parse(command);

    expect(result.isCommand(testCommand)).toBeFalsy();
});

test('Respect ignoreCase parameter', () => {
    const command = 'MyCommand';
    const testCommand = 'mYcOmManD';

    const parser = new CommandParser();
    const result = parser.parse(command);

    expect(result.isCommand(testCommand, true)).toBeTruthy();
});

test('Ignore whitespace around the command', () => {
    const command = '   ls   --param=value';
    const testCommand = 'ls';

    const parser = new CommandParser();
    const result = parser.parse(command);

    expect(result.isCommand(testCommand)).toBeTruthy();
});

test('Correctly parse subcommand', () => {
    const command = 'yarn add my_package --param=true';
    const testCommand = 'add';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const sub = result.subCommand();

    expect(sub.isCommand(testCommand)).toBeTruthy();
});

test('Interpret options with GNU syntax', () => {
    const command = 'ls --parameter value';
    const testKey = 'parameter';
    const testValue = 'value';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const parsedOptions = result.options();

    expect(parsedOptions.has(testKey)).toBeTruthy();
    expect(parsedOptions.get(testKey)).toBe(testValue);
});

test('Interpret options with GNU equals syntax', () => {
    const command = 'ls --parameter=value';
    const testKey = 'parameter';
    const testValue = 'value';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const parsedOptions = result.options();

    expect(parsedOptions.has(testKey)).toBeTruthy();
    expect(parsedOptions.get(testKey)).toBe(testValue);
});

test('Interpret single character option/flag', () => {
    const command = 'ls -p';
    const testKey = 'p';
    const testValue = 'true';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const parsedOptions = result.options();

    expect(parsedOptions.has(testKey)).toBeTruthy();
    expect(parsedOptions.get(testKey)).toBe(testValue);
});

test('Interpret single character option/flag with argument', () => {
    const command = 'ls -p myValue';
    const testKey = 'p';
    const testValue = 'myValue';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const parsedOptions = result.options();

    expect(parsedOptions.has(testKey)).toBeTruthy();
    expect(parsedOptions.get(testKey)).toBe(testValue);
});

test('Allow symbols with hyphens in them', () => {
    const command = 'ls symbol hello-world';
    const testFor = 'hello-world';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const symbols = result.symbols();

    expect(symbols.indexOf(testFor) >= 0).toBeTruthy();
});

test('Make sure symbols work in subcommands', () => {
    const command = 'yarn add gaming';
    const testFor = 'gaming';

    const parser = new CommandParser();
    const result = parser.parse(command);
    const subResult = result.subCommand();
    const symbols = subResult.symbols();

    expect(symbols.indexOf(testFor) >= 0).toBeTruthy();
});