import {BotModule, BotModuleFlowControl} from './bot-module';
import {Message, MessageAttachment, MessageReaction, PartialMessage, PartialUser, TextChannel, User} from 'discord.js';
import {rootLogger} from '../logging';
import {es} from '../connections/elasticsearch';

const log = rootLogger.child({component: 'emoji-contest'});

const channelIsEmojiContest = (channel: TextChannel) => {
    const channelName: string = channel.name;
    return channelName.endsWith('emoji-contest');
};

const rateMessage = async (message: Message | PartialMessage) => {
    if (message.partial) {
        message = await message.fetch();
    }

    const reactions = message.reactions.valueOf();
    let score = 0;

    for (const reaction of reactions.values()) {
        if (reaction.partial) {
            await reaction.fetch();
        }

        if (reaction.emoji.name === '👍') {
            score += reaction.count;

        } else if (reaction.emoji.name === '👎') {
            score -= reaction.count;
        }
    }

    return score;
};

const shouldDeleteMessage = (message: Message) => {
    const isServerOwner = message.author.id === message.guild.ownerId;
    const isModerator = message.member.roles.cache.some(role => role.name === 'Moderator');

    return !(isServerOwner || isModerator);
};

export class EmojiContestModule extends BotModule {
    async onMessage(message: Message, flowControl: BotModuleFlowControl): Promise<void> {
        if (!(message.channel instanceof TextChannel)) {
            return;
        }

        const channel: TextChannel = <TextChannel>message.channel;

        if (channelIsEmojiContest(channel)) {
            flowControl.halt();

            const attachments = message.attachments;
            if (attachments.size <= 0) {
                if (shouldDeleteMessage(message)) {
                    await message.delete();
                }

                return;
            }

            await Promise.all([
                message.react('👍'),
                message.react('👎')
            ]);
        }
    }

    private async onReaction(reaction: MessageReaction): Promise<void> {
        if (reaction.partial) {
            reaction = await reaction.fetch();
        }

        const message = reaction.message;
        if (!(message.channel instanceof TextChannel)) {
            return;
        }

        const channel: TextChannel = <TextChannel>message.channel;

        if (channelIsEmojiContest(channel)) {
            if (message.attachments.size <= 0) {
                return;
            }

            const getAttachmentData = () => {
                for (const attachment of message.attachments.values()) {
                    return {
                        name: attachment.name,
                        attachment: attachment.attachment.toString(),
                        url: attachment.url,
                        proxyUrl: attachment.proxyURL,
                        width: attachment.width,
                        height: attachment.height
                    };
                }
            };

            const rating = await rateMessage(message);
            log.info(`Rating for message ${message.id}: ${rating}`);

            await es().index({
                index: 'sparklee-emoji-contest-rating',
                id: message.id,
                body: {
                    guild_id: message.guild.id,
                    channel_id: channel.id,
                    rating: rating,
                    message_creation_datetime: message.createdAt.toISOString(),
                    attachment: getAttachmentData(),
                    '@timestamp': new Date().toISOString()
                }
            });

            const work = [];
            if (!(reaction.emoji.name === '👎' || reaction.emoji.name === '👍')) {
                try {
                    work.push(reaction.remove());

                } catch (_ignored) {
                    // Ignored
                }
            }

            await Promise.all(work);
        }
    }

    async onReactionAdded(reaction: MessageReaction, _user: User | PartialUser): Promise<void> {
        await this.onReaction(reaction);
    }

    async onReactionRemoved(reaction: MessageReaction, _user: User | PartialUser): Promise<void> {
        await this.onReaction(reaction);
    }
}
