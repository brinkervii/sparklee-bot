import { BotModulesService } from '../services/bot-modules.service';
import { ExampleBotModule } from './example';
import { EmojiContestModule } from './emoji-contest';

const registerBotModules = (): void => {
    const service = new BotModulesService();

    service.registerModule({
        id: 'example',
        name: 'Example',
        description: 'This is an example bot module',
        module: () => new ExampleBotModule()
    });

    service.registerModule({
        id: 'emoji-contest',
        name: 'Emoji Contest',
        description: 'Run an emoji contest in  your server!',
        module: () => new EmojiContestModule()
    });
};

export default registerBotModules;
