import {Message, MessageReaction, PartialMessageReaction, PartialUser, User} from 'discord.js';


export class BotModuleFlowControl {
    continueDispatch = true;

    halt(): void {
        this.continueDispatch = false;
    }
}

export abstract class BotModule {
    // eslint-disable-next-line @typescript-eslint/no-empty-function,@typescript-eslint/no-unused-vars
    async onMessage(message: Message, flowControl: BotModuleFlowControl): Promise<void> {

    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function,@typescript-eslint/no-unused-vars
    async onReactionAdded(reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser): Promise<void> {

    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function,@typescript-eslint/no-unused-vars
    async onReactionRemoved(reaction: MessageReaction | PartialMessageReaction, user: User | PartialUser): Promise<void> {

    }
}

export interface BotModuleSpecification {
    id: string
    name: string
    description: string,

    module(): BotModule
}
