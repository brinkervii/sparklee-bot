import {CommandParser} from './command-parser';

const parser = new CommandParser();
const result = parser.parse('ls ./hello-world --gamer true');

console.log(result);

const sub = result.subCommand();

console.log(sub);

type ValueTransformer = (s: string) => string | boolean | number;
type CommandArguments = Map<string, string>;
type CommandRunnable = (args: CommandArguments) => void;

interface CommandParameter {
    type: 'symbol' | 'symbol-array' | 'parameter' | 'flag';
    name: string | string[],
    description?: string,
    transformValue?: string | ValueTransformer;
}

interface CommandSpecification {
    cliEntry: string;
    description: string;
    subCommands?: CommandSpecification[];
    parameters?: CommandParameter[];
    run?: CommandRunnable
}

const registerCommand = (_spec: CommandSpecification) => null;

registerCommand({
    cliEntry: 'ls',
    description: 'List a directory'
});

registerCommand({
    cliEntry: 'yarn',
    description: 'Install node dependencies real good',
    subCommands: [
        {
            cliEntry: 'add',
            description: 'Add a node dependency to your project',
            parameters: [
                {
                    type: 'symbol-array',
                    name: 'package',
                    description: 'The package to be added to the project',
                },
                {
                    type: 'flag',
                    name: ['--dev', '-d'],
                    description: 'Save the dependency as a dev dependency',
                    transformValue: 'boolean'
                }
            ],

            run: args => {
                console.log(args.get('package'));
            }
        }
    ],
    run: _ => {
        console.log('Running yarn or something');
    }
});
