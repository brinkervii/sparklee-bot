
export interface TokenListEntry {
    tokenType: string;
    token: string
}

export interface DirectlyParsedData {
    command: string;
    rest: string;
    parsedRest?: {
        tokenList: TokenListEntry[];
        options: Map<string, string>
    }
}

export class ParseResult {
    constructor(private parsedData: DirectlyParsedData) {

    }

    isCommand(command: string, ignoreCase = false): boolean {
        let parsedCommand = this.parsedData.command;

        if (ignoreCase) {
            command = command.toLowerCase();
            parsedCommand = parsedCommand.toLowerCase();
        }

        return command.trim() === parsedCommand.trim();
    }

    options(): Map<string, string> {
        if (this.parsedData.parsedRest) {
            return this.parsedData.parsedRest?.options;
        }

        return new Map();
    }

    symbols(): string[] {
        if (this.parsedData.parsedRest) {
            return this.parsedData.parsedRest.tokenList
                .filter(entry => entry.tokenType === 'symbol')
                .map(entry => entry.token);
        }

        return [];
    }

    subCommand(): ParseResult {
        const parser = new CommandParser();
        return parser.parse(this.parsedData.rest);
    }
}

const parseRest = (rest: string) => {
    const bits = rest.split(/\s+/);

    const tokenList = bits.map((bit: string) => {
        let tokenType = 'symbol';

        if (bit.startsWith('--')) {
            tokenType = 'longOption';
            bit = bit.slice(2);

        } else if (bit.startsWith('-')) {
            tokenType = 'shortOption';
            bit = bit.slice(1);
        }

        return {
            tokenType: tokenType,
            token: bit
        };
    });

    const options = new Map();

    for (let i = 0; i < tokenList.length; i++) {
        const currentToken = tokenList[i];
        const nextToken = tokenList[i + 1];

        if (currentToken.tokenType === 'longOption') {
            const equalMatch = currentToken.token.match(/(.*)=(.*)/);

            if (equalMatch) {
                options.set(
                    equalMatch[1],
                    equalMatch[2]
                );

            } else if (nextToken && nextToken.tokenType === 'symbol') {
                options.set(currentToken.token, nextToken.token);
            }

        } else if (currentToken.tokenType === 'shortOption') {
            if (nextToken && nextToken.tokenType === 'symbol') {
                options.set(currentToken.token, nextToken.token);

            } else {
                options.set(currentToken.token, 'true');
            }
        }
    }

    return {
        tokenList: tokenList,
        options: options
    };
};

export class CommandParser {
    parse(userInput: string): ParseResult {
        const dpData: DirectlyParsedData = {
            command: '',
            rest: ''
        };

        let interpretedCommand = false;
        let gotNonWhitespaceCharacter;

        for (const character of userInput) {
            const isWhitespace = !!character.match(/\s+/);

            if (!isWhitespace) {
                gotNonWhitespaceCharacter = true;
            }

            if (interpretedCommand) {
                dpData.rest += character;

            } else {
                if (gotNonWhitespaceCharacter && isWhitespace) {
                    interpretedCommand = true;
                    continue;
                }

                dpData.command += character;
            }
        }

        dpData.parsedRest = parseRest(dpData.rest);

        return new ParseResult(dpData);
    }
}